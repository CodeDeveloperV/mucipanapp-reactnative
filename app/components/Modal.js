import PropTypes from 'prop-types';
import React from 'react';
import { Icon, Overlay } from 'react-native-elements';
import Object3D from './ModelView';

const Modal = ({modalVisible, closeModal}) => (
  <Overlay
    isVisible={modalVisible}
    onBackdropPress={closeModal}
    animationType="fade"
    height={600}
    width={330}>
    <>
      <Icon
        type="material"
        name="close"
        raised
        onPress={closeModal}
        containerStyle={{
          alignSelf: 'flex-end'
        }}
      />
      <Object3D />
    </>
  </Overlay>
);

Modal.propTypes = {
  modalVisible: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired
};

export default Modal;
