import React from 'react';
import { Animated, Platform, StyleSheet } from 'react-native';
import ModelView from 'react-native-gl-model-view';

const AnimatedModelView = Animated.createAnimatedComponent(ModelView);

export default class Object3D extends React.Component {
  constructor() {
    super();
    this.state = {
      rotateX: new Animated.Value(-80),
      rotateZ: new Animated.Value(0),

      fromXY: undefined,
      valueXY: undefined
    };
  }

  onMoveEnd = () => {
    this.setState({
      fromXY: undefined
    });
  };

  onMove = (e) => {
    const { pageX, pageY } = e.nativeEvent;
    const {rotateX, rotateZ, fromXY, valueXY} = this.state;
    if (!this.state.fromXY) {
      this.setState({
        fromXY: [pageX, pageY],
        valueXY: [rotateZ.__getValue(), rotateX.__getValue()]
      });
    } else {
      rotateZ.setValue(
        valueXY[0]
          + ((Platform.OS === 'ios' ? 1 : -1) * (pageX - fromXY[0])) / 2
      );
      rotateX.setValue(
        valueXY[1]
          + ((Platform.OS === 'ios' ? 1 : -1) * (pageY - fromXY[1])) / 2
      );
    }
  };

  render() {
    const { rotateZ, rotateX } = this.state;

    return (
      <AnimatedModelView
        model={{
          uri: 'demon.model'
        }}
        texture={{
          uri: 'demon.png'
        }}
        onStartShouldSetResponder={() => true}
        onResponderRelease={this.onMoveEnd}
        onResponderMove={this.onMove}
        animate
        tint={{
          r: 1.0, g: 1.0, b: 1.0, a: 1.0
        }}
        scale={0.01}
        translateZ={-2.5}
        rotateX={rotateX}
        rotateZ={rotateZ}
        style={styles.container}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent'
  }
});
