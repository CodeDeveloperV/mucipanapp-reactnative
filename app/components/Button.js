import React from 'react';
import { Button as RNEButton } from 'react-native-elements';

const Button = props => {
  return <RNEButton raised title={props.title} onPress={props.onPress} />;
};


export default Button;
