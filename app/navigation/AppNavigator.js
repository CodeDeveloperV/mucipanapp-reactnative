import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Home from '../screens/Home/Home';
import ObjectDetails from '../screens/Object/ObjectDetails';
import ObjectList from '../screens/Object/ObjectsList';
import Reports from '../screens/Report/Reports';

const AppNavigator = createStackNavigator(
  {
    Home: { screen: Home },
    Objects: { screen: ObjectList },
    Detail: { screen: ObjectDetails },
    Reports: { screen: Reports }
  },
  {
    initialRouteName: 'Home',
    // headerMode: 'none',
    defaultNavigationOptions: {
      header: null
    },
    mode: 'card'
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
