import moment from 'moment';
import { Body, Card, CardItem, Left, Text } from 'native-base';
import React from 'react';
import { Image, ScrollView, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import Modal from '../../components/Modal';

class ObjectDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false
    };
    this.modalVisibility = this.modalVisibility.bind();
  }

  modalVisibility = () =>
    this.setState({
      modalVisible: false
    });

  render() {
    const {navigation} = this.props;
    const {modalVisible} = this.state;
    return (
      <ScrollView>
        <Modal modalVisible={modalVisible} closeModal={this.modalVisibility}  />
        <Card>
          <CardItem header bordered>
            <Body>
              <Text style={styles.cardHeader}>
                {navigation.getParam('title') || 'Sin artista'}
              </Text>
              <Text style={styles.cardSubHeader}>
                {moment(navigation.getParam('acquisitiondate')).format('LL')}
              </Text>
            </Body>
          </CardItem>
          <CardItem bordered>
            <Body
              style={{
                flex: 1,
                alignItems: 'center'
              }}>
              <Image
                source={{
                  uri: `${navigation.getParam('image')}`
                }}
                style={styles.cardImage}
              />
            </Body>
          </CardItem>
          <CardItem>
            <Left>
              <Text style={styles.cardItemProperty}>Id</Text>
            </Left>
            <Body>
              <Text>{navigation.getParam('objectid')}</Text>
            </Body>
          </CardItem>
          <CardItem>
            <Left>
              <Text style={styles.cardItemProperty}># inventario</Text>
            </Left>
            <Body>
              <Text>{navigation.getParam('inventorynumber')}</Text>
            </Body>
          </CardItem>
          <CardItem>
            <Left>
              <Text style={styles.cardItemProperty}>Tipo</Text>
            </Left>
            <Body>
              <Text>{navigation.getParam('objecttype') || 'sin tipo'}</Text>
            </Body>
          </CardItem>
          <CardItem>
            <Left>
              <Text style={styles.cardItemProperty}>Colección</Text>
            </Left>
            <Body>
              <Text>
                {navigation.getParam('collectionname') || 'sin colección'}
              </Text>
            </Body>
          </CardItem>
          <CardItem>
            <Left>
              <Text style={styles.cardItemProperty}>Artista</Text>
            </Left>
            <Body>
              <Text>{navigation.getParam('artistname') || 'sin artista'}</Text>
            </Body>
          </CardItem>
          <CardItem>
            <Left>
              <Text style={styles.cardItemProperty}>Adquirida</Text>
            </Left>
            <Body>
              <Text>
                {moment(navigation.getParam('acquisitiondate')).format('LL')}
              </Text>
            </Body>
          </CardItem>
          <CardItem footer>
            <Left>
              <Text style={styles.cardItemProperty}>Valor</Text>
            </Left>
            <Body>
              <Text>
                {navigation.getParam('currentvalue')
                  ? navigation.getParam('currentvalue')
                  : 'Sin Valor'}
              </Text>
            </Body>
          </CardItem>
        </Card>
        <Button
          onPress={() => {
            this.setState({
              modalVisible: true
            });
          }}
          title="Pieza 3D"
          containerStyle={{
            alignSelf: 'center',
            marginBottom: 10
          }}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  buttonContainerStyle: {
    position: 'absolute',
    right: 10,
    top: 250,
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: '#0A5D56'
  },
  buttonStyle: {
    borderRadius: 50,
    width: 50,
    height: 50
  },
  cardHeader: {
    fontWeight: 'bold',
    fontSize: 18
  },
  cardSubHeader: {
    color: 'gray'
  },
  cardImage: {
    height: 180,
    width: 250,
    resizeMode: 'contain'
  },
  cardItemProperty: {
    fontWeight: 'bold'
  }
});
export default ObjectDetails;
