import { Dimensions, StyleSheet } from 'react-native';

const dimension = Dimensions.get('window');
const screenWidth = dimension.width;
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  sbContainerStyle: {
    backgroundColor: '#fff',
    borderBottomColor: '#fff',
    borderTopColor: '#fff',
    width: screenWidth
  },
  sbinputContainerStyle: {
    backgroundColor: '#0A5D56',
    height: 26
  },
  sbinputStyle: {
    fontSize: 18,
    color: '#fff'
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default styles;
