/* eslint-disable no-underscore-dangle */
import React from 'react';
import { ActivityIndicator, FlatList, Text, View } from 'react-native';
import { ListItem, SearchBar } from 'react-native-elements';
import styles from './Styles';

class ObjectList extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isLoadingMore: false,
      refreshing: false,
      offset: 0,
      page: 1,
      dataSource: [],
      search: ''
    };

    this.arrayholder = [];
    this.fetchData = this.fetchData.bind(this);
    this.handleRefresh = this.handleRefresh.bind(this);
    this.handleOnEndReached = this.handleOnEndReached.bind(this);
  }

  async componentDidMount() {
    this._isMounted = true;
    this.fetchData();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  fetchData = async () => {
    try {
      const URI = `http://mucipan-api.herokuapp.com/?limit=10&offset=${this.state.offset}`;
      const response = await fetch(URI);
      const responseJson = await response.json();
      const items = await responseJson;
      for (const index in items) {
        if (items[index].mainimageattachmentid) {
          const res = await fetch(
            `http://mucipan-api.herokuapp.com/${items[index].objectid}`,
          );
          const result = await res.text();
          items[index].imagen = result;
        }
      }
      this.setState({
        dataSource:
          this.state.page === 1 ? items : [...this.state.dataSource, ...items],
        isLoading: false,
        isLoadingMore: false,
        refreshing: false
      });
      this.arrayholder =
        this.state.page === 1 ? items : [...this.arrayholder, ...items];
    } catch (error) {
      this.setState({
        error
      });
    }
  };

  handleRefresh = () => {
    this.setState(
      {
        refreshing: true,
        page: 1,
        offset: 0
      },
      () => this.fetchData(),
    );
  };

  handleOnEndReached = () => {
    if (this.state.offset < 50) {
      this.setState(
        {
          isLoadingMore: true,
          page: this.state.page + 1,
          offset: this.state.offset + 10
        },
        () => this.fetchData(),
      );
    }
  };

  SearchFilterFunction(text) {
    const newData = this.arrayholder.filter(item => {
      const itemData = item.inventorynumber;
      const textData = text;
      return itemData.indexOf(textData) > -1;
    });
    this.state.dataSource = newData;
    this.state.search = text;
    this.setState({
      dataSource: newData,
      search: text
    });
  }

  renderFooter = () => {
    if (this.state.isLoadingMore) {
      return (
        <View>
          <ActivityIndicator animating size="large" />
        </View>
      );
    }
    return (
      <View>
        <Text />
      </View>
    );
  };

  renderItem = ({item}) => (
    <ListItem
      title={item.inventorynumber}
      titleStyle={{
        fontWeight: 'bold',
        fontFamily: 'sans-serif'
      }}
      subtitle={item.title || 'Sin titulo'}
      leftAvatar={{
        source: {
          uri:
            `data:image/jpeg;base64,${item.imagen}` ||
            'https://es.zenit.org/wp-content/uploads/2018/05/no-image-icon.png'
        },
        rounded: false,
        size: 'large',
        avatarStyle: {
          borderWidth: 2
        }
      }}
      onPress={() =>
        this.props.navigation.navigate('Detail', {
          objectid: item.objectid,
          inventorynumber: item.inventorynumber,
          objectype: item.objecttype,
          artistname: item.artistname,
          collectionname: item.collectionname,
          title: item.title,
          acquisitiondate: item.acquisitiondate,
          currentvalue: item.currentvalue,
          defaultcurrencytype: item.defaultcurrencytype,
          image:
            `data:image/jpeg;base64,${item.imagen}` ||
            'https://es.zenit.org/wp-content/uploads/2018/05/no-image-icon.png'
        })
      }
      bottomDivider
      chevron={{
        color: '#0A5D56',
        size: 20
      }}
    />
  );

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.loading} needsOffscreenAlphaCompositing>
          <ActivityIndicator size={90} color="#0A5D56" />
          <Text
            style={{
              color: '#0A5D56',
              fontSize: 20
            }}>
            Cargando piezas...
          </Text>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <SearchBar
          round
          containerStyle={styles.sbContainerStyle}
          inputContainerStyle={styles.sbinputContainerStyle}
          inputStyle={styles.sbinputStyle}
          searchIcon={{
            size: 20
          }}
          placeholder="Buscar..."
          placeholderTextColor="gray"
          onChangeText={text => this.SearchFilterFunction(text)}
          onClear={text => this.SearchFilterFunction('')}
          value={this.state.search}
        />
        <FlatList
          style={styles.container}
          data={this.state.dataSource}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderItem}
          showsVerticalScrollIndicator={false}
          onEndReachedThreshold={0.1}
          onEndReached={this.handleOnEndReached}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
          initialNumToRender={8}
          maxToRenderPerBatch={2}
          ListFooterComponent={this.renderFooter}
        />
      </View>
    );
  }
}

export default ObjectList;
