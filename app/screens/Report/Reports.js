import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { BarChart, LineChart } from 'react-native-chart-kit';

const dataLine = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June'],
  datasets: [
    {
      data: [20, 45, 28, 80, 99, 43],
      strokeWidth: 2 // optional
    }
  ]
};

class Reports extends React.Component {
  constructor(props) {
    super(props);
    this.arrayholder = [];
    this.newArray = [];
  }

  componentDidMount() {
    // this.fetchData();
  }

  fetchData = () => {
    fetch('http://mucipan-api.herokuapp.com/')
      .then((res) => res.json())
      .then((res) => (this.arrayholder = res));
  };

  render() {
    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View elevation={5} style={styles.header}>
          <Text style={styles.headerText}>Reporte de piezas</Text>
        </View>
        <LineChart
          data={dataLine}
          width={350}
          height={220}
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: '#0575E6',
            backgroundGradientTo: '#00F260',
            decimalPlaces: 0, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16
            }
          }}
          line
          style={styles.linechart}
        />
        <LineChart
          data={dataLine}
          width={350}
          height={220}
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: '#fb8c00',
            backgroundGradientTo: '#ffa726',
            decimalPlaces: 0, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16
            }
          }}
          bezier
          style={styles.linechart}
        />
        {/* <ProgressChart
          style={styles.linechart}
          data={dataPring}
          width={350}
          height={220}
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: '#0575E6',
            backgroundGradientTo: '#00F260',
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          }}
        /> */}
        <BarChart
          style={styles.linechart}
          data={dataLine}
          width={350}
          height={220}
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: '#0575E6',
            backgroundGradientTo: '#00F260',
            decimalPlaces: 0, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`
          }}
          verticalLabelRotation={30}
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  linechart: {
    marginVertical: 8,
    marginHorizontal: 5,
    borderRadius: 10
  },
  headerText: {
    fontSize: 23,
    fontWeight: 'bold',
    color: '#fff',
    marginVertical: 10
  },
  header: {
    flex: 0.26,
    flexDirection: 'row',
    // alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: '#0A5D56',
    width: 350,
    borderRadius: 20,
    marginHorizontal: 5,
    marginTop: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 0.2,
    position: 'relative'
  }
});

export default Reports;
