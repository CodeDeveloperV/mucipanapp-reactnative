import React from 'react';
import { Image, Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import images from '../../assets/img';
import styles from './Styles';


export default class Home extends React.PureComponent {
  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>Inventario de piezas</Text>
        </View>
        <View style={styles.logo_container}>
          <View elevation={8} style={styles.logo}>
            <Image source={images.logo} />
          </View>
        </View>
        <View style={styles.buttons_container}>
          <Button
            raised
            title="Objetos"
            onPress={() => this.props.navigation.navigate('Objects')}
          />
          <Button
            raised
            title="Reportes"
            onPress={() => this.props.navigation.navigate('Reports')}
          />
        </View>
      </View>
    );
  }
}
