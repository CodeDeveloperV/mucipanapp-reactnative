import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  headerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10
  },
  headerText: {
    fontSize: 38,
    // fontWeight: 'bold',
    color: '#0A5D56',
    textAlign: 'center',
    // letterSpacing: 0.2,
    fontFamily: 'Montserrat-Bold'
  },
  logo_container: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0, height: 4
    },
    shadowRadius: 5,
    shadowOpacity: 0.3,
    elevation: 8
  },
  buttons_container: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default styles;
