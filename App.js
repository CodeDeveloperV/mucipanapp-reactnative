import React from 'react';
import { StatusBar } from 'react-native';
import { ThemeProvider } from 'react-native-elements';
// Components
import AppContainer from './app/navigation/AppNavigator';

require('moment/locale/es-us');

const theme = {
  Button: {
    raised: true,
    type: 'solid',
    titleStyle: {
      color: '#fff',
      fontWeight: 'normal',
      fontSize: 23
    },
    containerStyle: {
      marginTop: 10,
      width: 220,
      height: 50
    },
    buttonStyle: {
      backgroundColor: '#0A5D56'
    }
  }
};

const App = () => (
  <ThemeProvider theme={theme}>
    <StatusBar
      barStyle="light-content"
      hidden={false}
      backgroundColor="#0A5D56"
      translucent={false}
    />
    <AppContainer />
  </ThemeProvider>
);

export default App;
