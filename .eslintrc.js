module.exports = {
  root: true,
  // extends: '@react-native-community',
  extends: ["airbnb", "prettier", "prettier/react"],
  parser: 'babel-eslint',
  "plugins": [
    "react",
    "jsx-a11y",
    "import",
    "eslint-plugin-prettier",
    "eslint-plugin-react"
  ],
  env: {
    "jest": true,
    "es6": true
  },
  rules: {
    'no-use-before-define': 'off',
    'react/jsx-filename-extension': [1, {extensions: ['.js', '.jsx']}],
    'comma-dangle': ['error', 'never'],
    'no-use-before-define': ['error', {variables: false}],
    'react/jsx-indent': ['error', 2],
    'react/jsx-indent-props': ['error', 2],
    'react/prop-types': 'off',
    'global-require': 'off',
    'react/state-in-constructor': 'off',
    'react/jsx-props-no-spreading': 'off',
    'react/static-property-placement': 'off',
    'react/jsx-curly-newline': 'off',
    'object-curly-newline': [
      'error',
      {
        ObjectExpression: 'always',
        ObjectPattern: {multiline: true},
        ImportDeclaration: 'never',
        ExportDeclaration: {multiline: true, minProperties: 3},
      },
    ],
    'react/destructuring-assignment': [0],
    "semi": ["error", "always"],
  },
  globals: {
    fetch: false,
  },
};
